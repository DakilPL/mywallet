function VisualsClass() {
    var touchX = 0;
    var touchStart = false;

    var leftMenu = false;
    var bottomMenu = false;
    var fabMenu = false;
    var popupMenu = false;

    var addOrDelete = VALUE_ADD;

    this.getAddOrDelete = function () {
        return addOrDelete;
    };

    this.menuMoveOut = function () {
        $('#fade').trigger('click');
    };

    this.bottomMenuMoveOutWithoutFade = function () {
        bottomMenu = false;
        $('.bottom-menu').animate({ 'bottom': '-400px' }, 200);
    }

    this.historyShow = function () {
        $('#main-menu').fadeOut(150);
        $('#accounts').fadeOut(150);
        ChangeSiteLeftMenu('main-menu');
        setTimeout(function () {
            $('#account-history').fadeIn(100);
        }, 150);
    };

    this.deleteDialogShow = function () {
        popupMenu = true;
        $('#delete-pop-up').fadeIn(200);
        $('#fade').fadeIn(200);
    };

    this.restoreDialogShow = function () {
        popupMenu = true;
        $('#restore-pop-up').fadeIn(200);
        $('#fade').fadeIn(200);
    }

    this.editDebtMoveIn = function () {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#adadad');
        }, 100)
        bottomMenu = true;
        $('#fade').fadeIn(200);
        $('#debt-edit-menu').animate({ 'bottom': '0px' }, 200);
    };

    function AddingMenuMoveIn() {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#adadad');
        }, 100)
        bottomMenu = true;
        $('#money-add-menu').animate({ 'bottom': '0px' }, 200);
        $('#main-fab').rotate({ duration: 500, angle: 135, animateTo: 0 });
        $('#add-money-fab').animate({ 'bottom': '-100px' }, 200);
    }

    function ChangeSiteLeftMenu(nameOnLeftMenu) {
        $('#left-' + nameOnLeftMenu).css({ 'background-color': '#fff' });
        $('#left-' + nameOnLeftMenu + '>div').css({ 'color': 'rgba(0, 0, 0, 0.87)' });
        $('#left-' + nameOnLeftMenu + '>i').css({ 'color': '#757575' });
    }

    function ChangeSite(menu) {
        $('#main-menu').fadeOut(200);
        ChangeSiteLeftMenu('main-menu');
        $('#accounts').fadeOut(200);
        ChangeSiteLeftMenu('accounts');
        $('#persons').fadeOut(200);
        ChangeSiteLeftMenu('persons');
        $('#debts').fadeOut(200);
        ChangeSiteLeftMenu('debts');
        $('#trash').fadeOut(200);
        ChangeSiteLeftMenu('trash');
        $('#account-history').fadeOut(200);

        setTimeout(function () {
            $('#' + menu).fadeIn(300);
        }, 200);
        $('#left-' + menu).css({ 'background-color': '#efe5fd' });
        $('#left-' + menu + '>div').css({ 'color': '#6002ee' });
        $('#left-' + menu + '>i').css({ 'color': '#6002ee' });

        $('#fade').trigger('click');
    }

    function ChangeTrashView(idOfView) {
        $('.trash-view').fadeOut(150);
        setTimeout(function () {
            $(idOfView).fadeIn(100);
        }, 150);
    }

    //blackIconsStatusbar
    document.addEventListener('deviceready', function () {
        StatusBar.styleDefault();
    }, false);

    //fade
    $('#fade').on('click', function () {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#ffffff');
        }, 100)

        if (leftMenu) {
            leftMenu = false;
            $('#left-menu').animate({ 'left': '-256px' }, 200);
            $('#fade').fadeOut(200);
        }

        if (bottomMenu) {
            bottomMenu = false;
            $('.bottom-menu').animate({ 'bottom': '-400px' }, 200);
            $('#fade').fadeOut(200);
        }

        if (fabMenu) {
            fabMenu = false;
            $('#main-fab').rotate({ duration: 500, angle: 135, animateTo: 0 });
            $('#add-money-fab').animate({ 'bottom': '-100px' }, 200);
            $('#fade').fadeOut(200);
            setTimeout(function () {
                $('#main-fab').css({ 'z-index': '1' });
            }, 200);
        }

        if (popupMenu) {
            popupMenu = false;
            $('.pop-up').fadeOut(200);
            $('#fade').fadeOut(200);
        }
    });

    //leftMenu
    $('#open-menu').on('click', function () {
        leftMenu = true;
        $('#left-menu').animate({ 'left': '0px' }, 200);
        $('#fade').fadeIn(200);
    });

    //leftMenu Touch
    $('body').on('touchstart', function (e) {
        if (!bottomMenu && !fabMenu && !touchStart) {
            if (!leftMenu && e.originalEvent.touches[0].clientX < 50) {
                touchX = e.originalEvent.touches[0].clientX;
                touchStart = true;
            } else if (leftMenu) {
                touchX = e.originalEvent.touches[0].clientX;
                touchStart = true;
            }
        }
    });

    $('body').on('touchmove', function (e) {
        if (touchStart) {
            if (!leftMenu && e.originalEvent.touches[0].clientX > touchX + 100) {
                $('#open-menu').trigger('click');
                touchStart = false;
            } else if (leftMenu && e.originalEvent.touches[0].clientX < touchX - 100) {
                $('#fade').trigger('click');
                touchStart = false;
            }
        }
    });

    $('body').on('touchend', function (e) {
        touchStart = false;
    });

    //bottomMenu
    $('.bottom-close').on('click', function () {
        $('#fade').trigger('click');
    });

    //focus text on inputs
    $('.text-input>input').on({
        focus: function () {
            $(this).parent().addClass('focused');
        },
        blur: function () {
            $(this).parent().removeClass('focused');
        }
    });

    $('.select').on({
        focus: function () {
            $(this).parent().addClass('focused');
        },
        blur: function () {
            $(this).parent().removeClass('focused');
        }
    });

    //fab
    $('#main-fab').on('click', function () {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#adadad');
        }, 100)
        if (!fabMenu) {
            fabMenu = true;
            $('#main-fab').rotate({ duration: 500, angle: 0, animateTo: 135 });
            $('#main-fab').css({ 'z-index': '3' });
            $('#add-money-fab').animate({ 'bottom': '95px' }, 200);
            $('#fade').fadeIn(200);
        } else {
            $('#fade').trigger('click');
        }
    });

    $('#account-fab').on('click', function () {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#adadad');
        }, 100)
        bottomMenu = true;
        $('#account-add-menu').animate({ 'bottom': '0px' }, 200);
        $('#fade').fadeIn(200);
    });

    $('#persons-fab').on('click', function () {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#adadad');
        }, 100)
        bottomMenu = true;
        $('#person-add-menu').animate({ 'bottom': '0px' }, 200);
        $('#fade').fadeIn(200);
    });

    $('#debts-fab').on('click', function () {
        setTimeout(function () {
            StatusBar.backgroundColorByHexString('#adadad');
        }, 100)
        bottomMenu = true;
        $('#debt-add-menu').animate({ 'bottom': '0px' }, 200);
        $('#fade').fadeIn(200);
    });

    //fab options
    $('#add-money-fab>.fab-add').on('click', function () {
        addOrDelete = VALUE_ADD;
        AddingMenuMoveIn();
        $('#money-add-or-remove').html('Dodawanie do salda');
    });

    $('#add-money-fab>.fab-remove').on('click', function () {
        addOrDelete = VALUE_SUB;
        AddingMenuMoveIn();
        $('#money-add-or-remove').html('Odejmowanie od salda');
    });

    //left menu buttons
    $('#left-main-menu').on('click', function () {
        ChangeSite('main-menu');
        $('.top-text').html('Mój portfel');
    });

    $('#left-accounts').on('click', function () {
        ChangeSite('accounts');
        $('.top-text').html('Konta');
    });

    $('#left-persons').on('click', function () {
        ChangeSite('persons');
        $('.top-text').html('Osoby');
    });

    $('#left-debts').on('click', function () {
        ChangeSite('debts');
        $('.top-text').html('Zadłużenia');
    });

    $('#left-trash').on('click', function () {
        ChangeSite('trash');
        $('.top-text').html('Kosz');
    });

    //bottom bar - trash
    $('#trash>.bottom-bar>.bottom-bar-button').on('click', function () {
        var $this = $(this);
        $('#trash>.bottom-bar>.bottom-bar-button').animate({ 'padding-top': '16px' }, 150);
        $this.animate({ 'padding-top': '8px' }, 150);
        setTimeout(function () {
            $('#trash>.bottom-bar>.bottom-bar-button').removeClass('bottom-bar-selected');
            $this.addClass('bottom-bar-selected');
        }, 200);
    });

    $('#trash-bottom-accounts').on('click', function () {
        ChangeTrashView('#trash-accounts');
    });

    $('#trash-bottom-history').on('click', function () {
        ChangeTrashView('#trash-history');
    });

    $('#trash-bottom-persons').on('click', function () {
        ChangeTrashView('#trash-persons');
    });

    $('#trash-bottom-debts').on('click', function () {
        ChangeTrashView('#trash-debts');
    });

    //history close
    $('#history-back-button').on('click', function () {
        $('#account-history').fadeOut(150);

        setTimeout(function () {
            $('#accounts').fadeIn(100);
        }, 150);

        $('.top-text').html('Konta');

        $('#left-accounts').css({ 'background-color': '#efe5fd' });
        $('#left-accounts>div').css({ 'color': '#6002ee' });
        $('#left-accounts>i').css({ 'color': '#6002ee' });

    });
}

var visuals = new VisualsClass();
