function DebtsClass() {

    var self = this;
    var debtToEdit = null;
    var valueToEdit = null;

    this.DebtsScreen = function () {
        sql.getDebts(function (debts) {
            $('#debts-view > *').not('.prototype').remove();

            for (i in debts) {
                var clone = $('#debts-view .prototype').clone(true);
                clone.removeClass('prototype');
                if (debts[i].type === DEBT_FOR_ME) {
                    clone.find('.debt-person').html(debts[i].name + ' pożyczył(a) mi');
                } else if (debts[i].type === DEBT_FOR_OTHER) {
                    clone.find('.debt-person').html('Pożyczyłem dla ' + debts[i].name);
                }
                clone.find('.debt-value').html(debts[i].value.toFixed(2) + ' PLN');
                clone.data('id', debts[i].id);
                clone.data('value', debts[i].value);
                clone.data('person_id', debts[i].person_id);
                clone.show();
                $('#debts-view').append(clone);
            }
        });

        sql.getPeople(function (people) {
            $('#debt-person > *').not('.prototype').remove();

            for (i in people) {
                var clone = $('#debt-person .prototype').clone(true);
                clone.removeClass('prototype');
                clone.html(people[i].name);
                clone.attr('value', people[i].id);
                clone.show();
                $('#debt-person').append(clone);
            }
        });
    }

    sql.initialize(function () {
        self.DebtsScreen();
    }, true);

    $('#add-debt-accept').on('click', function () {
        var personId = $('#debt-person').val();
        var value = $('#debt-value').val();
        var type = $('#debt-me-or-not').val();

        sql.addDebts(function () {
            self.DebtsScreen();
            visuals.menuMoveOut();

            $('#debt-person').val('');
            $('#debt-value').val('');
        }, personId, value, type);
    });

    $('.debt-edit').on('click', function () {
        visuals.editDebtMoveIn();
        debtToEdit = $(this).closest('.once-debt').data('id');
        valueToEdit = $(this).closest('.once-debt').data('value');
    });

    $('#edit-debt-accept').on('click', function () {
        var valueAfterEdit = Number(valueToEdit) + Number($('#debt-edit-value').val());

        sql.editDebts(function () {
            self.DebtsScreen();
            visuals.menuMoveOut();

            $('#debt-edit-value').val('');
        }, valueAfterEdit, debtToEdit);
    });

    $('#delete-debt-accept').on('click', function () {
        itemToDelete = DEBT;
        deletingId = debtToEdit;

        visuals.bottomMenuMoveOutWithoutFade();
        visuals.deleteDialogShow();
    })

    $('#delete-item-button').on('click', function () {
        if (itemToDelete === DEBT) {
            sql.deleteDebts(function () {
                deletingId = null;
                itemToDelete = null;

                visuals.menuMoveOut();
                self.DebtsScreen();
                trash.trashScreen();
            }, deletingId);
        }
    });

}

var debts = null;

document.addEventListener('deviceready', function () {
    debts = new DebtsClass();
}, false);