function HistoryClass() {

    var account_id = null;
    var account_name = null;

    function GetAccountHistory() {
        sql.getHistory(function (history) {
            $('#history-view > *').not('.prototype').remove();

            $('.top-text').html('Historia - ' + account_name);

            visuals.historyShow();

            for (var i in history) {
                var clone = $('#history-view .prototype').clone(true);

                clone.removeClass('prototype');

                if (history[i].value >= 0) {
                    clone.find('.history-value').html("+" + history[i].value.toFixed(2) + " PLN");
                } else {
                    clone.find('.history-value').html(history[i].value.toFixed(2) + " PLN");
                }

                clone.find('.history-name').html(history[i].name);
                clone.data('id', history[i].id);
                clone.show();
                $('#history-view').append(clone);

            }

        }, account_id);

    }

    $('.history-button').on('click', function () {
        account_id = $(this).closest('.once-account').data('id');
        account_name = $(this).closest('.once-account').data('name');

        GetAccountHistory();
    });

    $('.history-delete').on('click', function () {
        deleting_id = $(this).closest('.once-history').data('id');
        itemToDelete = HISTORY;

        visuals.deleteDialogShow();

    });

    $('#delete-item-button').on('click', function () {
        if (itemToDelete === HISTORY) {
            sql.deleteHistory(function () {
                deleting_id = null;
                itemToDelete = null;

                visuals.menuMoveOut();
                GetAccountHistory();
                main.MainScreen();
                accounts.AccountsScreen();
                trash.trashScreen();

            }, deleting_id);
        }

    });

}

var history = null;

document.addEventListener('deviceready', function () {
    history = new HistoryClass();
}, false);