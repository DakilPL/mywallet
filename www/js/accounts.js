function AccountsClass() {

    var self = this;

    this.AccountsScreen = function () {
        sql.getAccountBalance(function (accounts) {
            $('#accounts-view > *').not('.prototype').remove();

            for (var i in accounts) {
                var clone = $('#accounts-view .prototype').clone(true);
                clone.removeClass('prototype');
                clone.find('.account-name').html(accounts[i].name);
                clone.find('.account-value').html(accounts[i].balance.toFixed(2) + ' PLN');
                clone.data('id', accounts[i].id);
                clone.data('name', accounts[i].name);
                clone.show();
                $('#accounts-view').append(clone);
            }
        });

    };

    sql.initialize(function () {
        self.AccountsScreen();
    }, false);

    $('#add-account-accept').on('click', function () {
        var newAccountName = $('#add-account-title').val();

        sql.addAccount(function () {
            self.AccountsScreen();
            visuals.menuMoveOut();
            main.MainScreen();

            $('#add-account-title').val('');
        }, newAccountName);

    });

    $('.account-delete').on('click', function () {
        deleting_id = $(this).closest('.once-account').data('id');
        itemToDelete = ACCOUNT;

        visuals.deleteDialogShow();

    });

    $('#delete-item-button').on('click', function () {
        if (itemToDelete === ACCOUNT) {
            sql.deleteAccount(function () {
                self.AccountsScreen();
                main.MainScreen();
                visuals.menuMoveOut();
                trash.trashScreen();

                deleting_id = null;
                itemToDelete = null;
            }, deleting_id);
        }

    });

}

var accounts = null;

document.addEventListener('deviceready', function () {
    accounts = new AccountsClass();
}, false);