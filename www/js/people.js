function PeopleClass() {

    var self = this;

    this.GetPeople = function () {
        sql.getPeople(function (people) {
            $('#people-view > *').not('.prototype').remove();

            for (i in people) {
                var clone = $('#people-view .prototype').clone(true);
                clone.removeClass('prototype');
                clone.find('.person-name').html(people[i].name);
                clone.data('id', people[i].id);
                clone.show();
                $('#people-view').append(clone);
            }
        });
    }

    sql.initialize(function () {
        self.GetPeople();
    }, true);

    $('#add-person-accept').on('click', function () {
        var personName = $('#add-person-title').val();

        sql.addPeople(function () {
            self.GetPeople();
            visuals.menuMoveOut();
            debts.DebtsScreen();

            $('#add-person-title').val('');
        }, personName);
    });

    $('.person-delete').on('click', function () {
        deleting_id = $(this).closest('.once-person').data('id');
        itemToDelete = PERSON;

        visuals.deleteDialogShow();
    });

    $('#delete-item-button').on('click', function () {
        if (itemToDelete === PERSON) {
            sql.deletePeople(function () {
                deleting_id = null;
                itemToDelete = null;

                visuals.menuMoveOut();
                self.GetPeople();
                debts.DebtsScreen();
                trash.trashScreen();

            }, deleting_id);
        }

    });

}

var people = null;

document.addEventListener('deviceready', function () {
    people = new PeopleClass();
}, false);