const VALUE_ADD = 0;
const VALUE_SUB = 1;

const ACCOUNT = 0;
const HISTORY = 1;
const PERSON = 2;
const DEBT = 3;

const DEBT_FOR_ME = 0;
const DEBT_FOR_OTHER = 1;