function SqlClass() {
    var connect = null;
    function createArray(rs) {
        var arr = new Array();
        var l = rs.rows.length;
        for (var i = 0; i < l; i++) {
            arr[i] = rs.rows.item(i);
        }
        return arr;
    }
    this.initialize = function (fn, checkDatabase) {
        connect = window.sqlitePlugin.openDatabase({ name: 'myWallet.db', location: 'default' });
        if (checkDatabase) {
            connect.transaction(function (tr) {
                tr.executeSql(
                    "SELECT COUNT(*) AS is_accounts FROM sqlite_master WHERE type='table' AND name='accounts'",
                    [],
                    function (tr, rs) {
                        if (rs.rows.item(0).is_accounts == 0) {
                            tr.executeSql("CREATE TABLE accounts(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, is_deleted INTEGER)");
                            tr.executeSql("CREATE TABLE history(id INTEGER PRIMARY KEY AUTOINCREMENT, created_date DATETIME, account_id INTEGER, value DECIMAL(10,2), name TEXT, is_deleted INTEGER, FOREIGN KEY (account_id) REFERENCES accounts(id))");
                            tr.executeSql("CREATE TABLE people(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, is_deleted INTEGER)");
                            tr.executeSql("CREATE TABLE debts(id INTEGER PRIMARY KEY AUTOINCREMENT, created_date DATETIME, people_id INTEGER, value INTEGER, is_deleted INTEGER, type INTEGER, FOREIGN KEY (people_id) REFERENCES people(id))");
                        }
                    }
                );
            }, function (error) {
                alert('Błąd krytyczny bazy danych');
            }, function () {
                if (typeof fn == 'function') {
                    fn();
                }
            });
        } else {
            if (typeof fn == 'function') {
                fn();
            }
        }
    };

    this.getDeletedAccounts = function (fn) {
        connect.executeSql(
            'SELECT `id`, `name` FROM `accounts` WHERE `is_deleted` = 1 ORDER BY `id` DESC',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getDeletedHistory = function (fn) {
        connect.executeSql(
            'SELECT `history`.`id`, `accounts`.`name`, `history`.`value`, `history`.`name` FROM `history` \
            INNER JOIN `accounts` ON `history`.`account_id` = `accounts`.`id` \
            WHERE `history`.`is_deleted` = 1 AND `accounts`.`is_deleted` = 0 ORDER BY `history`.`id` DESC',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getDeletedPeople = function (fn) {
        connect.executeSql(
            'SELECT `id`, `name`, `type` FROM `people` WHERE `is_deleted` = 1 ORDER BY `id` DESC',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getDeletedDebts = function (fn) {
        connect.executeSql(
            'SELECT `debts`.`id`, `people`.`name`, `debts`.`value`, `debts`.`type`, `people`.`type` AS person_type FROM `debts` \
            INNER JOIN `people` ON `debts`.`people_id` = `people`.`id` \
            WHERE `debts`.`is_deleted` = 1 AND `people`.`is_deleted` = 0 ORDER BY `debts`.`id` DESC',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };

    this.getBalance = function (fn) {
        connect.executeSql(
            'SELECT COALESCE(SUM(`value`), 0) AS balance FROM `history` \
            INNER JOIN `accounts` ON `history`.`account_id` = `accounts`.`id` \
            WHERE `history`.`is_deleted` = 0 AND `accounts`.`is_deleted` = 0',
            [],
            function (rs) {
                fn(rs.rows.item(0).balance);
            }
        );
    };
    this.getAccountBalance = function (fn) {
        connect.executeSql(
            'SELECT `accounts`.`id`, `accounts`.`name`, COALESCE(SUM(`history`.`value`), 0) AS balance FROM `accounts` \
            LEFT JOIN `history` ON `history`.`account_id` = `accounts`.`id` AND `history`.`is_deleted` = 0 \
            WHERE `accounts`.`is_deleted` = 0 \
            GROUP BY `accounts`.`id`',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getAccounts = function (fn) {
        connect.executeSql(
            'SELECT `name`, `id` FROM `accounts` \
            WHERE `accounts`.`is_deleted` = 0',
            [],
            function (rs) {
                fn(createArray(rs));
            }
        );
    };
    this.addAccount = function (fn, name) {
        connect.executeSql(
            'INSERT INTO `accounts`(`name`, `is_deleted`) VALUES (?,0)',
            [name],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.deleteAccount = function (fn, id) {
        connect.executeSql(
            'UPDATE `accounts` SET `is_deleted` = 1 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.restoreAccount = function (fn, id) {
        connect.executeSql(
            'UPDATE `accounts` SET `is_deleted` = 0 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.restoreHistory = function (fn, id) {
        connect.executeSql(
            'UPDATE `history` SET `is_deleted` = 0 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.addMoneyAccount = function (fn, id, value, name) {
        connect.executeSql(
            'INSERT INTO `history`(`created_date`, `account_id`, `value`, `name`, `is_deleted`) VALUES (DATETIME("now"), ?, ?, ?, 0)',
            [id, value, name],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getPeople = function (fn) {
        connect.executeSql(
            'SELECT `name`, `id` FROM `people` \
            WHERE `people`.`is_deleted` = 0',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.addPeople = function (fn, name) {
        connect.executeSql(
            'INSERT INTO `people`(`name`, `is_deleted`) VALUES (?,0)',
            [name],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.deletePeople = function (fn, id) {
        connect.executeSql(
            'UPDATE `people` SET `is_deleted` = 1 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.restorePeople = function (fn, id) {
        connect.executeSql(
            'UPDATE `people` SET `is_deleted` = 0 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getDebts = function (fn) {
        connect.executeSql(
            'SELECT `people_id`, `people`.`name`, `value`, `debts`.`type`, `debts`.`is_deleted`, `debts`.`id` FROM `debts` \
            INNER JOIN `people` ON `people_id` = `people`.`id` AND `people`.`is_deleted` = 0 \
            WHERE `debts`.`is_deleted` = 0',
            [],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.addDebts = function (fn, people, value, type) {
        connect.executeSql(
            'INSERT INTO `debts`(`created_date`, `people_id`, `value`, `type`, `is_deleted`) VALUES (DATETIME("now"),?,?,?,0)',
            [people, value, type],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.deleteDebts = function (fn, id) {
        connect.executeSql(
            'UPDATE `debts` SET `is_deleted` = 1 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.editDebts = function (fn, new_value, id) {
        connect.executeSql(
            'UPDATE `debts` SET `value` = ? WHERE `id` = ?',
            [new_value, id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.restoreDebts = function (fn, id) {
        connect.executeSql(
            'UPDATE `debts` SET `is_deleted` = 0 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getHistory = function (fn, account_id) {
        connect.executeSql(
            'SELECT `history`.`id`, `history`.`value`, `history`.`name` FROM `history` \
            INNER JOIN `accounts` ON `history`.`account_id` = `accounts`.`id`\
            WHERE `history`.`account_id` = ? AND `history`.`is_deleted` = 0 AND `accounts`.`is_deleted` = 0\
            ORDER BY `history`.`id` DESC',
            [account_id],
            function (rs) {
                fn(createArray(rs));
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.deleteHistory = function (fn, id) {
        connect.executeSql(
            'UPDATE `history` SET `is_deleted` = 1 WHERE `id` = ?',
            [id],
            function (rs) {
                fn();
            },
            function (e) {
                console.log(e);
            }
        );
    };
    this.getLastHistory = function (fn) {
        connect.executeSql(
            'SELECT `history`.`name`, `history`.`value` FROM `history` \
            WHERE `history`.`is_deleted` = 0 \
            ORDER BY `history`.`id` DESC \
            LIMIT 1',
            [],
            function (rs) {
                fn(rs.rows.item(0).name, rs.rows.item(0).value);
            },
            function (e) {
                console.log(e);
            }
        );
    }
};
var sql = new SqlClass();