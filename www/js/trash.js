function TrashClass() {

    var self = this;

    this.trashScreen = function () {
        sql.getDeletedAccounts(function (accounts) {
            $('#trash-accounts > *').not('.prototype').remove();

            for (var i in accounts) {
                var clone = $('#trash-accounts .prototype').clone(true);
                clone.removeClass('prototype');
                clone.find('.account-name').html(accounts[i].name);
                clone.data('id', accounts[i].id);
                clone.show();
                $('#trash-accounts').append(clone);
            }
        });

        sql.getDeletedHistory(function (history) {
            $('#trash-history > *').not('.prototype').remove();

            for (var i in history) {
                var clone = $('#trash-history .prototype').clone(true);

                clone.removeClass('prototype');

                if (history[i].value >= 0) {
                    clone.find('.history-value').html("+" + history[i].value.toFixed(2) + " PLN");
                } else {
                    clone.find('.history-value').html(history[i].value.toFixed(2) + " PLN");
                }

                clone.find('.history-name').html(history[i].name);
                clone.data('id', history[i].id);
                clone.show();
                $('#trash-history').append(clone);
            }
        });

        sql.getDeletedPeople(function (people) {
            $('#trash-persons > *').not('.prototype').remove();

            for (i in people) {
                var clone = $('#trash-persons .prototype').clone(true);
                clone.removeClass('prototype');
                clone.find('.person-name').html(people[i].name);
                clone.data('id', people[i].id);
                clone.show();
                $('#trash-persons').append(clone);
            }
        });

        sql.getDeletedDebts(function (debts) {
            $('#trash-debts > *').not('.prototype').remove();

            for (i in debts) {
                var clone = $('#trash-debts .prototype').clone(true);
                clone.removeClass('prototype');
                if (debts[i].type === DEBT_FOR_ME) {
                    clone.find('.debt-person').html(debts[i].name + ' pożyczył(a) mi');
                } else if (debts[i].type === DEBT_FOR_OTHER) {
                    clone.find('.debt-person').html('Pożyczyłem dla ' + debts[i].name);
                }
                clone.find('.debt-value').html(debts[i].value.toFixed(2) + ' PLN');
                clone.data('id', debts[i].id);
                clone.data('value', debts[i].value);
                clone.data('person_id', debts[i].person_id);
                clone.show();
                $('#trash-debts').append(clone);
            }
        });
    }

    sql.initialize(function () {
        self.trashScreen();
    }, true);

    $('.account-restore').on('click', function () {
        itemToRestore = ACCOUNT;
        restoringId = $(this).closest('.once-account').data('id');
        visuals.restoreDialogShow();
    });

    $('.history-restore').on('click', function () {
        itemToRestore = HISTORY;
        restoringId = $(this).closest('.once-history').data('id');
        visuals.restoreDialogShow();
    });

    $('.person-restore').on('click', function () {
        itemToRestore = PERSON;
        restoringId = $(this).closest('.once-person').data('id');
        visuals.restoreDialogShow();
    });

    $('.debt-restore').on('click', function () {
        itemToRestore = DEBT;
        restoringId = $(this).closest('.once-debt').data('id');
        visuals.restoreDialogShow();
    });

    $('#restore-item-button').on('click', function () {
        if (itemToRestore === ACCOUNT) {
            sql.restoreAccount(function () {
                self.trashScreen();
                main.MainScreen();
                accounts.AccountsScreen();
                visuals.menuMoveOut();

                itemToRestore = null;
                restoringId = null;
            }, restoringId);
        }
        if (itemToRestore === HISTORY) {
            sql.restoreHistory(function () {
                self.trashScreen();
                main.MainScreen();
                accounts.AccountsScreen();
                visuals.menuMoveOut();

                itemToRestore = null;
                restoringId = null;
            }, restoringId);
        }
        if (itemToRestore === PERSON) {
            sql.restorePeople(function () {
                people.GetPeople();
                self.trashScreen();
                visuals.menuMoveOut();

                itemToRestore = null;
                restoringId = null;
            }, restoringId);
        }
        if (itemToRestore === DEBT) {
            sql.restoreDebts(function () {
                self.trashScreen();
                debts.DebtsScreen();
                visuals.menuMoveOut();

                itemToRestore = null;
                restoringId = null;
            }, restoringId);
        }
    });

}

var trash = null;

document.addEventListener('deviceready', function () {
    trash = new TrashClass();
}, false);