function MainClass() {

    var self = this;

    this.MainScreen = function () {
        sql.getBalance(function (totalBalance) {
            $('#money-summary').html(totalBalance.toFixed(2) + ' PLN');
        });

        sql.getAccountBalance(function (accounts) {
            $('#main-screen-accounts-view > *').not('.prototype').remove();

            for (var i in accounts) {
                var clone = $('#main-screen-accounts-view .prototype').clone(true);
                clone.removeClass('prototype');
                clone.find('.account-name').html(accounts[i].name);
                clone.find('.account-value').html(accounts[i].balance.toFixed(2) + ' PLN');
                clone.data('id', accounts[i].id);
                clone.data('name', accounts[i].name);
                clone.show();
                $('#main-screen-accounts-view').append(clone);
            }
        });

        sql.getAccounts(function (accounts) {
            $('#add-select-account > *').not('.prototype').remove();

            for (var i in accounts) {
                var clone = $('#add-select-account .prototype').clone(true);
                clone.removeClass('prototype');
                clone.attr('value', accounts[i].id);
                clone.html(accounts[i].name);
                clone.show();
                $('#add-select-account').append(clone);
            }
        });

        sql.getLastHistory(function (name, value) {
            if (value >= 0) {
                $('.last-history-element').html(name + ': +' + value + ' PLN');
            } else {
                $('.last-history-element').html(name + ': ' + value + ' PLN');
            }
        });

    }

    sql.initialize(function () {
        self.MainScreen();
    }, true);

    $('#add-money-accept').on('click', function () {
        var id = $('#add-select-account').val();
        var value = $('#add-number').val();
        var name = $('#add-title').val();

        sql.addMoneyAccount(function () {
            self.MainScreen();
            accounts.AccountsScreen();
            visuals.menuMoveOut();

            $('#add-number').val('');
            $('#add-title').val('');
        }, id, value, name);
    });

}

var main = null;

document.addEventListener('deviceready', function () {
    main = new MainClass();
}, false);