var itemToDelete = null;
var deletingId = null;

var itemToRestore = null;
var restoringId = null;

$('#delete-cancel-button').on('click', function () {
    itemToDelete = null;
    deletingId = null;
    visuals.menuMoveOut();
});

$('#restore-cancel-button').on('click', function () {
    itemToRestore = null;
    restoringId = null;
    visuals.menuMoveOut();
});